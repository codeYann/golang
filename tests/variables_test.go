package tests

import (
	"testing"

	"github.com/codeYann/golang/fundamentals"
)

func TestVars(t *testing.T) {
	i, j, k := fundamentals.GetVars()
	if i != 10 && j != 15.0 && k != 30 {
		t.Log("Error!")
	}
}
