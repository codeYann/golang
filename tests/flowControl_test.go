package tests

import (
	"github.com/codeYann/golang/fundamentals"
	"testing"
)

func testControlFlowCase(t *testing.T) {
	number := -1
	msg := fundamentals.TestCases(number)

	if msg != "value is not in [0, 10) or [10, 20]" {
		t.Log("Error")
	}
}
