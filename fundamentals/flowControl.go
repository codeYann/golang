package fundamentals

func TestCases(value int) string {
	var message string

	if value >= 0 && value < 10 {
		message = "value in [0, 10)"
	} else if value >= 10 && value <= 20 {
		message = "value in [10, 20)"
	} else {
		message = "value is not in [0, 10) or [10, 20]"
	}

	println(message)
	return message
}
