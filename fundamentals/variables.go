// Package fundamentals is a
package fundamentals

var a,
	b,
	c = 10, 15.0, 30

// GetVars returns three vars defined in global scope
func GetVars() (int, float64, int) {
	return a, b, c
}
